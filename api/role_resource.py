from flask_restplus import Namespace, Resource

api = Namespace('roles', description='operations related to roles')


@api.route('/')
class RoleListResource(Resource):

    def get(self):
        return None, 200

    def post(self):
        return None, 200
