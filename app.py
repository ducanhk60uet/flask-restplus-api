from flask import Flask
from flask_restplus import Api
from api.user_resource import api as user_api
from api.role_resource import api as role_api

app = Flask(__name__)

api = Api(app, title='api document',
          version='version 1.2',
          contact_email='ducanhk60uet@gmail.com',
          description='description: flask restplus demo application'
          )

api.add_namespace(user_api)
api.add_namespace(role_api)

if __name__ == '__main__':
    app.run(debug=True)
