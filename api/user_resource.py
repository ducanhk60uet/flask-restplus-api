import random

from flask_restplus import Resource, Namespace
from utilities.parser import default_parser

api = Namespace('users', description='operations related to users')

data: list = [
    {
        'user_id': 1,
        'user_name': 'A',
        'age': 17
    },
    {
        'user_id': 2,
        'user_name': 'B',
        'age': 20
    },
    {
        'user_id': 3,
        'user_name': 'C',
        'age': 5
    }
]


@api.route('/<int:user_id>')
class UserResource(Resource):

    @api.response('200', 'success')
    @api.response('400', 'user not found')
    def get(self, user_id):
        """get user by id"""

        global data
        user = next(filter(lambda l: l['user_id'] == user_id, data), None)
        if not user:
            return {'message': 'user not found'}, 400
        return user, 200

    def delete(self, user_id):
        """delete user by id"""

        global data
        user = next(filter(lambda l: l['user_id'] == user_id, data), None)
        if not user:
            return {'message', 'invalid user_id'}, 400

        # delete user
        data.remove(user)
        return {'message': 'deleted user successfully', 'user': user}, 200


@api.route('/')
class UserListResource(Resource):

    def search(self, users, name, age_from, age_to):
        if name:
            users = filter(lambda l: l['user_name'] == name, users)
        if age_to:
            users = filter(lambda l: age_to >= l['age'], users)
        if age_from:
            users = filter(lambda l: age_from <= l['age'], users)
        users = list(users)
        return users

    get_parser = default_parser() \
        .add_argument('name', type=str, help='name of user') \
        .add_argument('age_from', type=int, help='min age') \
        .add_argument('age_to', type=int, help='max age') \
        .add_argument('gender', choices=['male', 'female', "all"], default="all", help='gender of users', required=True)

    @api.response('200', 'success')
    @api.response('400', 'user not found')
    @api.expect(get_parser)
    def get(self):
        """get users filtered by conditions"""

        global data
        args = self.get_parser.parse_args()
        print(args)
        name = args['name']
        age_from = args['age_from']
        age_to = args['age_to']

        users = data
        users = self.search(users, name, age_from, age_to)
        if not users:
            return {'message': 'users not found'}, 400
        return users, 200

    post_parser = default_parser() \
        .add_argument('name', type=str, required=True, location='form') \
        .add_argument('age', type=int, required=True, location='form')

    @api.response('201', 'created user successfully')
    @api.response('400', 'error')
    @api.expect(post_parser)
    def post(self):
        """create user"""

        global data
        args = self.post_parser.parse_args()
        user = {
            'user_id': random.randint(20, 1000),
            'user_name': args['name'],
            'age': args['age']
        }
        data.append(user)
        return {'message': 'created user successfully', 'user': user}, 201
