from flask_restplus import reqparse
from flask_restplus.reqparse import RequestParser


def default_parser() -> RequestParser:
    return reqparse.RequestParser()


def page_parser() -> RequestParser:
    parser = reqparse.RequestParser()
    parser.add_argument('page_index', type=int, location='args')
    parser.add_argument('page_size', type=int, location='args')
    parser.add_argument('order_by', type=str, location='args')
    parser.add_argument('order_type', choices=('desc', 'asc'), location='args')
    return parser
